import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'
import { rgb_to_hex, hex_to_rgb } from '../src/converter.js'

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img id="imageId" src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <h1>Colour converter!</h1>
    <h3>You can convert rgb to hex</h3>
    <h3>And hex to rgb.</h3> 
    <h3>You can allso see colour you entered last as background colour of logo.</h3>
    <div class="rgbhex">
      <input id="rgbREDInput" type="text" placeholder="Enter red color">
      <input id="rgbGREENInput" type="text" placeholder="Enter green color">
      <input id="rgbBLUEInput" type="text" placeholder="Enter blue color">
      <button id="rgbtohex" type="button">Convertert rgb to hex</button>
      <p id="rgbResponse"></p>
    </div>
    <div class="hexrgb">
      <input id="hexInput" type="text" placeholder="Enter HEX color">
      <button id="hextorgb" type="button">Convertert hex to rgb</button>
      <p id="hexResponse"></p>
    </div>
  </div>
`

// Etsi kuva elementti
const imageElement = document.getElementById('imageId');

// Etsitään painikkeet ja syötekentät
const rgbToHexButton = document.getElementById("rgbtohex");

// Lisätään tapahtumankäsittelijä painikkeelle "rgbtohex"
rgbToHexButton.addEventListener("click", async () => {
    // Haetaan RGB-väriarvo syötekentästä
    const rgbREDColor = rgbREDInput.value;
    const rgbGREENColor = rgbGREENInput.value;
    const rgbBLUEColor = rgbBLUEInput.value;

    // Luodaan kyselyosoite ja lisätään RGB-väriarvo siihen
    const apiUrl = `http://localhost:3000/api/v1/rgb-to-hex?red=${encodeURIComponent(rgbREDColor)}&green=${encodeURIComponent(rgbGREENColor)}&blue=${encodeURIComponent(rgbBLUEColor)}`;

    try {
        // Lähetetään kysely
        const response = await fetch(apiUrl);
        
        // Tarkistetaan vastauksen tila
        if (response.ok) {
            // Vastausteksti
            const text = await response.text();
            document.getElementById('rgbResponse').textContent = text;
            console.log(text);
            // Muuta taustaväriä
            imageElement.style.backgroundColor = text;
        } else {
            console.error('Failed to fetch data:', response.status);
        }
    } catch (error) {
        console.error('Error fetching data:', error);
    }
});


// Etsitään dokumentista hex muuntimen button
const hexToRgbButton = document.getElementById("hextorgb");

// Lisätään tapahtumakäsittelijä painikkeelle hextorgb
hexToRgbButton.addEventListener("click", async () => {
  // Etsitään dokumentista hex muuntimen input
  const hexInput = document.getElementById("hexInput");
  const hexColor = hexInput.value;
  // Luodaan kyselyosoite ja lisätään RGB-väriarvo siihen
  const apiUrl = `http://localhost:3000/api/v1/hex-to-rgb?hex=%23${encodeURIComponent(hexColor)}`;

  try {
    // Lähetetään kysely
    const response = await fetch(apiUrl);
    
    // Tarkistetaan vastauksen tila
    if (response.ok) {
        // Vastausteksti
        const text = await response.text();
        document.getElementById('hexResponse').textContent = text;
        console.log(text);
        imageElement.style.backgroundColor = "#" + hexColor;
    } else {
        console.error('Failed to fetch data:', response.status);
    }
  } catch (error) {
    console.error('Error fetching data:', error);
  }
});